;; First Install packages
(defvar home-directory "~/.emacs.d")
(add-to-list 'load-path (concat home-directory "/site-lisp"))

(require 'install-packages)
(require 'initialization)

(require 'init-org)
(require 'init-elixir)
(require 'init-lisp)
(require 'init-c)
(require 'init-erlang)
(require 'init-ruby)
(require 'init-stat)
(require 'init-scala)
(require 'init-java)
(require 'init-web)
;(require 'init-editing)
(global-set-key "\C-cd" 'dash-at-point)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(eclim-eclipse-dirs (quote ("/Applications/Eclipse.app/Contents/Eclipse")))
 '(eclim-executable "/Applications/Eclipse.app/Contents/Eclipse/eclim")
 '(flycheck-clang-language-standard "c++11")
 '(org-agenda-files (quote ("~/workspace/GTD/org-mode/gtd.org")))
 '(writeroom-restore-window-config t))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
