;; init-elixir.el

(defun custom-elixir-hook ()
  (alchemist-mode))
(add-hook 'elixir-mode-hook 'custom-elixir-hook)

(provide 'init-elixir)
