;; init-java.el

(custom-set-variables
  '(eclim-eclipse-dirs '("/Applications/Eclipse.app/Contents/Eclipse"))
  '(eclim-executable "/Applications/Eclipse.app/Contents/Eclipse/eclim"))

(require 'eclim)
(global-eclim-mode)

(require 'company-emacs-eclim)
(company-emacs-eclim-setup)

(require 'eclimd)

(provide 'init-java)
