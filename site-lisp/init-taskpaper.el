(load-file "~/.emacs.d/site-lisp/taskpaper.el/taskpaper.el")
(require 'taskpaper-mode)
(add-to-list 'auto-mode-alist '("\\.taskpaper\\'" . taskpaper-mode))

(provide 'init-taskpaper)
