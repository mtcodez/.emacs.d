;;   init-web.el

(install-packs 'emmet-mode)
(install-packs 'ac-emmet)

(fset 'html-mode 'nxml-mode)
(add-hook 'nxml-mode-hook 'emmet-mode)

(provide 'init-web)
