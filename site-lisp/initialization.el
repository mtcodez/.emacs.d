;; initialization.el

                                        ;(load-theme 'zenburn t)
(load-theme 'monokai t)


(setenv "PATH" (concat (getenv "PATH")))
(setq exec-path (append exec-path '("/Users/mattluo/bin")))

(ido-mode 1)
(setq ido-everywhere t)

;; company-mode
(company-mode t)
(add-hook 'after-init-hook 'global-company-mode)

;; Appearance
(tool-bar-mode 0)
(scroll-bar-mode 0)

;; Font size
;(set-default-font "Monaco 15")
(set-face-attribute 'default nil :height 150)

;; Misc
(setq make-backup-files nil)
(electric-pair-mode 1)
(defalias 'yes-or-no-p 'y-or-n-p)
(global-set-key (kbd "C-x C-b") 'ibuffer)

(blink-cursor-mode t)
(setq-default cursor-type 'bar)
;(setq cursor-type 'bar)

;; Prefer space to tab
(setq-default indent-tabs-mode nil)

;; Company
(setq company-tooltip-limit 10)                      ; bigger popup window
(setq company-idle-delay .2)                         ; decrease delay before autocompletion popup shows
(setq company-echo-delay 0)                          ; remove annoying blinking
(setq company-begin-commands '(self-insert-command)) ; start autocompletion only after typing

;; Flyspell
                                        ;(add-hook 'LaTeX-mode-hook '(lambda () flyspell-mode))
(add-hook 'latex-mode-hook 'flyspell-mode)
(add-hook 'markdown-mode-hook '(lambda () flyspell-mode))
;; XML
(setq nxml-slash-auto-complete-flag t)

(when (memq window-system '(mac ns))
  (exec-path-from-shell-initialize))

;; fix the PATH variable
;; (defun set-exec-path-from-shell-PATH ()
;;   (let ((path-from-shell (shell-command-to-string "TERM=vt100 $SHELL -i -c 'echo $PATH'")))
;;     (setenv "PATH" path-from-shell)
;;     (setq exec-path (split-string path-from-shell path-separator))))
;; (when window-system (set-exec-path-from-shell-PATH))

(define-key global-map "\M-/" 'comment-region)

;; Helm
(require 'helm-config)
(helm-mode 1)
(helm-autoresize-mode 1)

(global-set-key (kbd "M-x") 'helm-M-x)
(global-set-key (kbd "C-x C-f") 'helm-find-files)
(global-set-key (kbd "C-x b") 'helm-buffers-list)

;; Line number
(global-linum-mode 1)

(define-key global-map (kbd "C-c SPC") 'ace-jump-mode)
(define-key global-map (kbd "C-x SPC") 'ace-jump-mode-pop-mark)

;;
(show-paren-mode t)
;;

(setq next-line-add-newlines t)

(delete-selection-mode 1)
(column-number-mode 1)

;; tramp
(setq tramp-default-method "ssh")
(provide 'initialization)
