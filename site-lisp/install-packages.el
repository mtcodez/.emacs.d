;; install-packages.el
(require 'package)
(package-initialize)
(setq package-archives '(("gnu" . "http://elpa.gnu.org/packages/")
			 ("marmalade" . "http://marmalade-repo.org/packages/")
			 ("melpa" . "http://melpa.milkbox.net/packages/")))

(defmacro install-packs (package-name)
  `(unless (package-installed-p ,package-name)
     (package-install ,package-name)))
                                       ;(package-refresh-contents)

(defconst custom-packages
  '(color-theme
    monokai-theme
    company
    company-c-headers
    helm
    helm-ag
    exec-path-from-shell
    ggtags
    company-irony
    irony
    anzu
    projectile
    helm-projectile
    clean-aindent-mode
    comment-dwim-2
    dtrt-indent
    ws-butler
    iedit
    volatile-highlights
    undo-tree
    zygospore
    smartparens
    duplicate-thing
    lua-mode
    emmet-mode
    ))

(install-packs 'color-theme)
(install-packs 'zenburn-theme)
(install-packs 'monokai-theme)
(install-packs 'darcula-theme)
(install-packs 'company)
(install-packs 'company-c-headers)
(install-packs 'clojure-mode)
;(install-packs 'ac-cider)
(install-packs 'helm)
(install-packs 'helm-ag)
(install-packs 'go-mode)
(install-packs 'ace-jump-mode)
(install-packs 'org-pomodoro)

(add-to-list 'package-pinned-packages '(cider . "melpa-stable") t)
(install-packs 'cider)

(install-packs 'exec-path-from-shell)
(install-packs 'elixir-mode)

(add-to-list 'package-pinned-packages '(alchemist . "melpa-stable") t)
(install-packs 'alchemist)

(install-packs 'markdown-mode)
(install-packs 'erlang)
(install-packs 'ggtags)

(install-packs 'irony)
(install-packs 'company-irony)
(install-packs 'flycheck-irony)
(install-packs 'irony-eldoc)

(install-packs 'yasnippet)

(install-packs 'emacs-eclim)

(install-packs 'erlang)
(install-packs 'edts)

;; ruby
(install-packs 'robe)


(defun install-packages ()
  "Install all required packages."
  (interactive)
  (unless package-archive-contents
    (package-refresh-contents))
  (dolist (package custom-packages)
    (unless (package-installed-p package)
      (package-install package))))

(install-packages)

(provide 'install-packages)
